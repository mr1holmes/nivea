package com.mr1holmes.nivea;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by mr1holmes on 30/8/16.
 */
public class ImageOpAdapter extends RecyclerView.Adapter<ImageOpAdapter.ViewHolder> implements FragmentCallback {

    private ArrayList<String> mData;
    private Context mContext;
    private OnItemClickListener mItemClickListener;
    public static int IMAGE_COUNT = 0;

    public ImageOpAdapter(Context context, ArrayList<String> data) {
        mData = data;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_image_preview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String filepath = mData.get(position);
        if (!filepath.equals(Util.EMPTY_LABEL)) {
            File file = new File(filepath);
            if (file.exists()) {
                holder.imageView.setImageURI(null);
                holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.imageView.setImageURI(Uri.fromFile(file));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBitmapReceived(int position, String filepath) {
        mData.set(position, filepath);
        notifyItemChanged(position, filepath);
        if (IMAGE_COUNT < 4) IMAGE_COUNT++;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout placeholder;
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.source_image);
            placeholder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            placeholder.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(imageView, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ImageView view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
