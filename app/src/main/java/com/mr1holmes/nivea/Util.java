package com.mr1holmes.nivea;

import android.os.Environment;

import java.io.File;

/**
 * Created by mr1holmes on 10/8/16.
 */
public class Util {

    private static final String TAG = Util.class.getSimpleName();
    public static final String EMPTY_LABEL = "nofile";
    public static boolean INSTRUCTION_SHOWN = false;

    public static File getOutputMediaFile(int position) {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nivea");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + position + ".jpg");

        return mediaFile;
    }

    public static String getAppropriateMessage() {
        if (ImageOpAdapter.IMAGE_COUNT == 3) {
            return "You need to capture 1 more image";
        } else {
            return "You need to capture " + (4 - ImageOpAdapter.IMAGE_COUNT) + " more images";
        }
    }
}
