package com.mr1holmes.nivea;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by mr1holmes on 30/8/16.
 */
public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.registration_frag_container, new RegistrationFragment())
                .commit();
    }
}
