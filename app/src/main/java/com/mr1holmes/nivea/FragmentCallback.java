package com.mr1holmes.nivea;

/**
 * Created by mr1holmes on 31/8/16.
 */
public interface FragmentCallback {
    void onBitmapReceived(int position, String filepath);
}
