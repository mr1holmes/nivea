package com.mr1holmes.nivea;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by mr1holmes on 30/8/16.
 */
public class RegistrationFragment extends Fragment {

    private Button button;
    private TextInputEditText firstNameEditText;
    private TextInputEditText lastNameEditText;
    private TextInputEditText fatherNameEditText;
    private TextInputEditText rollNumberEditText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);

        firstNameEditText = (TextInputEditText) rootView.findViewById(R.id.firstname_edittext);
        lastNameEditText = (TextInputEditText) rootView.findViewById(R.id.lastname_edittext);
        fatherNameEditText = (TextInputEditText) rootView.findViewById(R.id.fathername_edittext);
        rollNumberEditText = (TextInputEditText) rootView.findViewById(R.id.rollnumber_edittext);
        button = (Button) rootView.findViewById(R.id.next_btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ImageOperationsActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
