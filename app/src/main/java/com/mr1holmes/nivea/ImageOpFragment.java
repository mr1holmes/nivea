package com.mr1holmes.nivea;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mr1holmes on 30/8/16.
 */
public class ImageOpFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private ImageOpAdapter mAdapter;
    private ArrayList<String> mData;
    private CoordinatorLayout mCoordinatorLayout;
    private Button mButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image_operations, container, false);
        mCoordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinatorLayout);
        mButton = (Button) rootView.findViewById(R.id.send_btn);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.image_grid);
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);

        mData = new ArrayList<>();
        mData.add(Util.EMPTY_LABEL);
        mData.add(Util.EMPTY_LABEL);
        mData.add(Util.EMPTY_LABEL);
        mData.add(Util.EMPTY_LABEL);


        mAdapter = new ImageOpAdapter(getActivity(), mData);

        mRecyclerView.setAdapter(mAdapter);

        ImageOpAdapter.OnItemClickListener onItemClickListener = new ImageOpAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(ImageView view, int position) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // here requestCode = position
                    startActivityForResult(takePictureIntent, position);
                }
            }
        };

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ImageOpAdapter.IMAGE_COUNT == 4) {
                    // upload images to server
                    Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "Uploading...", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(mCoordinatorLayout, Util.getAppropriateMessage(), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        mAdapter.setOnItemClickListener(onItemClickListener);

        if (!Util.INSTRUCTION_SHOWN) {
            Util.INSTRUCTION_SHOWN = true;
            new MaterialDialog.Builder(getActivity())
                    .title(R.string.popup_label)
                    .content(R.string.popup_inst_content)
                    .positiveText(R.string.popup_label_positive)
                    .show();
        }

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            File file = Util.getOutputMediaFile(requestCode);
            if (file == null) {
                return;
            }
            String filePath = file.getPath();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mAdapter.onBitmapReceived(requestCode, filePath);
        }
    }
}